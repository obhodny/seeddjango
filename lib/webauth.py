#
import uuid
from django.contrib.auth.hashers import is_password_usable,  check_password, make_password
import django.db
from lib.database import UserStorage



class MyUser(object):
  ID_FIELD = None
  PASSWORD_FIELD = 'password'
  IS_ACTIVE_FIELD = 'is_active'
  SESSION_ID_FIELD = 'session_id'
  LAST_LOGIN_FIELD = 'last_login'
  NAME_FIELD = 'username'
  IS_ADMIN_FIELD = 'is_admin'
  IS_OWNER_FIELD = 'is_owner'

  def __init__(self, data, storage):
    self._data = data
    self._storage = storage
    self.ID_FIELD = storage.id_field

  is_active = property(
    lambda self: self._data[self.IS_ACTIVE_FIELD]
  )
  is_admin = property(
    lambda self: self._data[self.IS_ADMIN_FIELD]
  )
  #is_owner = property(
  #  lambda self: self._data[self.IS_OWNER_FIELD]
  #)
  pk = property(lambda self: self._data[self.SESSION_ID_FIELD])
  last_login = property(
    lambda self: self._data[self.LAST_LOGIN_FIELD],
    lambda self, value: self._data.update(**{ self.LAST_LOGIN_FIELD: value })
  )
  name = property(lambda self: self._data[self.NAME_FIELD])

  def is_authenticated(self):
    return True

  def get_id(self):
    return self._data[self.ID_FIELD]

  def check_password(self, password):
#    return self._data[self.PASSWORD_FIELD] == password
    return check_password(password, self._data[self.PASSWORD_FIELD])

  def save(self, *args, **kwargs):
    data = dict(self._data)
    id = data.pop(self.ID_FIELD)
    self._storage.save(id, data)

  def __del__(self):
#    self._storage.close()
    self._storage = None

class AuthBackend(object):
  def __init__(self):
    self._storage = UserStorage(connect=django.db.connections['main'])

  def _add_user(self, __data=None, **kwargs):
    data = __data and __data or kwargs
    data.setdefault('password', '')
    data.setdefault('session_id', uuid.uuid4().hex)
    user_id = self._storage.add(data, commit=True)
    user = self._storage.get_by_id(user_id, as_dict=True)
    return user

  def _save_user(self, user_id, __data=None, **kwargs):
    data = __data and __data or kwargs
    self._storage.save(user_id, data)
    user = self._storage.get_by_id(user_id, as_dict=True)
    return user

  def authenticate(self, username=None, password=None):
    data = self._storage.get_by_name(username)
    if data:
      user = MyUser(data, self._storage)
      if not user.check_password(password):
        user = None
    else:
      user = None
    return user

  def get_user(self, session_id):
#    print 'get_user.session_id:', repr(session_id)
    data = self._storage.get_by_session_id(session_id)
    if data:
      user = MyUser(data, self._storage)
    else:
      user = None
    return user

class WebAuth(object):
  def __init__(self, storage):
    self.storage = storage

  def add_user(self, data, commit=False):
    data['password'] = make_password(data['password'])
    id = self.storage.add(data, commit=True)
    data = self.storage.get_by_id(id)
    if data:
      user = MyUser(data, self.storage)
    else:
      user = None
    return user

  def is_name_exists(self, name):
#    res = self.storage.exists(dict(name=name))
    res = self.storage.get_by_name(name)
    return res is not None
