#
import sys
import os
import traceback
import math
import json
import time
import datetime
import uuid
import urllib
import json

import logging
import config
#import  seeddjango.settings

#log = logging.getLogger('django.request')
URL_TEMPLATE = 'http://maps.googleapis.com/maps/api/geocode/json?%s'
DEFAULT_IMAGE_EXT = 'jpg'

class DictObject(object):
  def __init__(self, __data=None, **kwargs):
    self._data = dict(__data) if __data else {}
    self._data.update(kwargs)

  def __getitem__(self, key):
    return self._data[key]

  def __getattr__(self, name):
    try:
      return self[name]
    except KeyError:
      raise AttributeError('A instance has no attribute %r' % name)

_SIZES_NAME = [
  "Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"
]

def print_exception():
  traceback.print_exc(file=sys.stderr)

def log_message(*args):
#  text = '\t'.join(args) + '\n'
  text = time.strftime('[%Y-%m-%d %H:%M:%S] ')
  text += '\t'.join(args) + '\n'
  sys.stdout.write(text)

def get_exception_text():
  return traceback.format_exc()

def remove_file(file_name):
  if os.path.isfile(file_name):
    try:
      os.remove(file_name)
    except:
      print_exception()

def get_form_errors(form, as_dict=False):
  res = [ (name, value.as_text()) for name, value in form.errors.items() ]
  if as_dict:
    res = dict(res)
  return res

def format_form_errors(form):
#  res = '\n'.join(
#    [ "%s %s" % (name, value.as_text()) for name, value in form.errors.items() ]
#  )
  res = '\n'.join([ "%s %s" % item for item in get_form_errors(form) ])
  return res

def get_form_data(form, booleans=None, skip_fields=None):
  data = [
    (name, value) for name, value in form.cleaned_data.items() if value is not None
  ]
  if skip_fields:
   data = [ item for item in data if item[0] not in skip_fields ]
  data = dict(data)
  if booleans:
    for name in booleans:
      if name in data:
        data[name] = bool(data[name])
  return data



def format_size(size):
  if size:
    index = int(math.floor(math.log(size, 1024)))
    for i in xrange(index):
      size = size / 1024
  else:
    index = 0
  res = "%i %s" % (size, _SIZES_NAME[index])
  return res

class DateEncoder(json.JSONEncoder):
  def default(self, obj):
    if isinstance(obj, datetime.date):
      res = obj.strftime('%Y-%m-%d')
    else:
      res = json.JSONEncoder.default(self, obj)
    return res

def json_dumps(data, **kwargs):
  kwargs = dict(kwargs, cls=DateEncoder)
  return json.dumps(data, **kwargs)


def to_unicode(text):
  if isinstance(text, str):
    text = unicode(text, 'utf-8')
  return text

def to_utf8(text):
  if isinstance(text, unicode):
    text = text.encode('utf-8')
  return text















