#
from webauth import storage

def get_users():
  res = storage.get_users()
  return res

def search_users(text):
  res = storage.search_users(text)
  return res

def get_user_info(id):
  return storage.get_user_info(id)

def update_user_info_storage(id, data):
  storage.with_transaction(storage.update_by_id, id, data)

def delete_user_storage(id):
  storage.with_transaction(storage.delete_by_id, id)
