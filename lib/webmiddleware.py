#
import logging
from django.middleware.csrf import CsrfViewMiddleware as _CsrfViewMiddleware
from django.http.request import UnreadablePostError

log = logging.getLogger('django.request')

REASON_UNREADABLE_POST_ERROR = "Unreadable Post Error."

class CsrfViewMiddleware(_CsrfViewMiddleware):
  def process_view(self, request, callback, callback_args, callback_kwargs):
#    log.error('Middleware.csrf.process_view:')
    try:
      return _CsrfViewMiddleware.process_view(
        self, request, callback, callback_args, callback_kwargs
      )
    except UnreadablePostError:
      log.error('Middleware.csrf.process_view: %s', REASON_UNREADABLE_POST_ERROR)
      return self._reject(request, REASON_UNREADABLE_POST_ERROR)
