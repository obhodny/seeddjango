#
import operator
import logging
import MySQLdb as mysql
import database_config as config
from utils import print_exception, to_utf8
from datetime import datetime, date

#log = logging.getLogger('django.request')
import datetime

def db_connect():
  connect = mysql.connect(
    host=config.host, db=config.database, 
    user=config.user, passwd=config.password,
    charset=config.charset
  )
  return connect

class AS_IS(object):
  def __init__(self, value):
    self.value = value

VIDEO_CATEGORY_ORDER_MAP = dict(
      popularity='popularity', duration='duration', recent='save_timestamp'
)
DEFAULT_VIDEO_CATEGORY_ORDER = 'popularity'

class BaseStorage(object):
  AS_IS = AS_IS
  TABLE_NAME = None
  ID_FIELD = 'id'
  PARAM_CHAR = '%s'
  TRUE = 1
  FALSE = 0

  def __init__(self, table=None, id_field=None, connect=None):
    self._connect = None
    self._cursor  = None
#    self._execute = None
    self.reconnect(connect)
    if table is None:
      table = self.TABLE_NAME
    if id_field is None:
      id_field = self.ID_FIELD
    self._table = table
    self._id_field = id_field

  id_field = property(lambda self: self._id_field)

  def _close_cursor(self):
#    self._execute = None
    self._cursor.close()
    self._cursor = None

  def _execute(self, *args, **kwargs):
    try:
      self._cursor.execute(*args, **kwargs)
    except Exception, error:
      if repr(error).find('InterfaceError') >= 0:
        self._cursor = self._connect.cursor()
        self._cursor.execute(*args, **kwargs)
      else:
        raise error

  def reconnect(self, connect=None):
    if connect is None:
      connect = db_connect()
    self._connect = connect
    self._cursor  = self._connect.cursor()
#    self._execute = self._cursor.execute

  def _prepare_where(self, where):
    where_array = []; where_values = []
    if where:
      if   isinstance(where, dict):
        where_array = []
        for name, value in where.items():
          w1 = "%s = %s" % (name, self.PARAM_CHAR)
          where_array.append(w1)
          where_values.append(value)
        where = " AND ".join(where_array)
      elif not isinstance(where, basestring):
        where = None
    else:
      where = None
    return (where, where_values)

  def _prepare_dict_items(self, data, cursor):
    descriptions = [ desc[0] for desc in cursor.description ]
    res = []
    for item in data:
      item2 = dict([
        (name, value) for name, value in map(None, descriptions, item) 
      ])
      res.append(item2)
    return res

  def _prepare_dict_item(self, data, cursor):
    if data:
      res = dict([ 
        (desc[0], value) for desc, value in map(None, cursor.description, data)
      ])
    else:
      res = None
    return res

  def close(self):
    self._close_cursor()
    self._connect.close()
    self._connect = None

  def with_transaction(self, function, *args, **kwargs):
    self.begin()
    error = None
    try:
      res = function(*args, **kwargs)
    except Exception, error:
      print_exception()
    if error:
      self.rollback()
      raise error
    else:
      self.commit()
    return res

  def begin(self):
    self._execute('BEGIN')

  def commit(self):
    self._execute('COMMIT')

  def rollback(self):
    self._execute('ROLLBACK')

  def delete(self, where=None, debug=False):
    command = "DELETE FROM %s" % self._table
    where, where_values = self._prepare_where(where)
    if where:
      command += " WHERE %s" % where
    if debug:
      print command
      print 'values:', where_values
    self._execute(command, where_values)

  def insert(self, data, debug=False):
    names = data.keys(); values = data.values()
    names = ", ".join(names)
    holders = ", ".join([self.PARAM_CHAR]*len(values))
    command = 'INSERT INTO %s (%s) VALUES (%s)' % (
      self._table, names, holders
    )
    if debug:
      print command, values
    self._execute(command, tuple(values))
    return self._cursor.lastrowid

  def update(self, data, where, debug=False):
    sets_array = []; values = []
    for name, value in data.items():
      if isinstance(value, self.AS_IS):
        sets_array.append("%s = %s" % (name, value.value))
      else:
        sets_array.append("%s = %s" % (name, self.PARAM_CHAR))
        values.append(value)
    sets = ", ".join(sets_array)
    command = "UPDATE %s SET %s" % (self._table, sets)
    if where:
      where, where_values = self._prepare_where(where)
      command += " WHERE %s" % where
      values.extend(where_values)
    if debug:
      print command, values
    self._execute(command, values)

  def get_by_id(self, id, select=None, as_dict=False, debug=False):
    return self.get({ self._id_field: id }, select, as_dict=as_dict, debug=debug)

  def update_by_id(self, id, data, debug=False):
    self.update(data, { self._id_field: id }, debug=debug)

  def delete_by_id(self, id, debug=False):
    self.delete({ self._id_field: id }, debug=debug)

  def get(self, where, select=None, order_by=None, as_dict=False, table=None, debug=False):
    if select is None:
      select = '*'
    table = table if table else self._table
    command = "SELECT %s FROM %s" % (select, table)
    where, where_values = self._prepare_where(where)
    if where:
      command += " WHERE %s" % where
    if order_by:
      command += " ORDER BY %s" % order_by
    command += " LIMIT 1"
    if debug:
      print command
      print 'values:', where_values
    self._execute(command, where_values)
    res = self._cursor.fetchone()
    if res and as_dict:
      res = self._prepare_dict_item(res, self._cursor)
    return res

  def getall(self, where=None, select=None, order_by=None, limit=None, join=None,
    distinct=False, as_dict=False, debug=False
  ):
    if select is None:
      select = '*'
    distinct = 'DISTINCT' if distinct else ''
    command = "SELECT %s %s FROM %s" % (distinct, select, self._table)
    where, where_values = self._prepare_where(where)
    if join:
      if isinstance(join, str):
        join = 'JOIN %s' % join
      else:
        join_array = []
        for item in join:
          if isinstance(join, str):
            item = 'JOIN %s' % item
          else:
            item = 'JOIN %s ON %s' % tuple(item)
          join_array.append(item)
        join = '\n'.join(join_array)
      command += '\n %s' % join
    if where:
      command += " WHERE %s" % where
    if order_by:
      command += " ORDER BY %s" % order_by
    if limit:
      command += " LIMIT %s" % limit
    if debug:
      print command
      print 'values:', where_values
#    log.error('sql: %s', command)
    self._execute(command, where_values)
    res = self._cursor.fetchall()
    if res and as_dict:
      res = self._prepare_dict_items(res, self._cursor)
    return res

  def get_all(self, *args, **kwargs):
    return self.getall(*args, **kwargs)

  def get_list(self, where, field=None, debug=False, **kwargs):
    if field is None:
      field = self._id_field
    data = self.getall(where, field, debug=debug, **kwargs)
    data = [ item[0] for item in data ]
    return data

  def exists(self, where, field=None):
    field = field if field else self._id_field
    res = self.get(where, field)
    if res is not None:
      res = res[0]
    return res

  def clear_table(self):
#    self.delete()
    self.with_transaction(self.delete)

  def _write_data(self, data):
    insert = self.insert
    for item in data:
      insert(item)

  def write_data(self, data):
    self.with_transaction(self._write_data, data)

  def update_data(self, data):
    return self.with_transaction(self._update_data, data)

  def execute(self, command, values=None):
    if values is not None:
      self._execute(command, values)
    else:
      self._execute(command)
    return self._cursor

  def execute_and_fetch(self, command, values=None, as_dict=False):
    cursor = self.execute(command, values)    
    res = cursor.fetchall()
    if res and as_dict:
      res = self._prepare_dict_items(res, cursor)
    return res

  def add(self, __data=None, **kwargs):
    data = __data and __data or kwargs
    return self.with_transaction(self.insert, data)

  def save(self, id, __data=None, **kwargs):
    data = __data and __data or kwargs
#    print 'save:', repr(id), repr(data)
    return self.with_transaction(self.update_by_id, id, data)




class UserStorage(BaseStorage):
  TABLE_NAME = 'User'
  ID_FIELD = 'idUser'

  def get_by_name(self, name):
    res = self.get(dict(username=name), as_dict=True)
    return res

  def get_by_session_id(self, session_id):
    res = self.get(dict(session_id=session_id), as_dict=True)
    return res

  def get_by_auth_provider(self, auth_provider, auth_id):
    res = self.get(dict(auth_provider=auth_provider, auth_id=auth_id), as_dict=True)
    return res

  def save(self, id, data, debug=False):
#    print 'save:', repr(id), repr(data)
    self.with_transaction(self.update_by_id, id, data, debug=debug)

  def add(self, data, commit=False):
    if commit:
      id = self.with_transaction(self.insert, data)
    else:
      id = self.insert(data)
    return id

  def get_users(self):
    res = self.get_all(None,
      'idUser AS id, username AS name, email', order_by='name',
      as_dict=True
    )
    return res

  def get_user_info(self, id):
    res = self.get_by_id(id, 'idUser AS id, username AS name, email, avatar, background, quote, website, instagram, twitter, facebook, google_p, is_active, is_admin', as_dict=True)
    return res

  def get_user_info2(self, id):
    res = self.get_by_id(id, 'idUser AS id, username AS name, avatar', as_dict=True)
    return res


  def search_users(self, text, limit=None, debug=False):
    limit = limit and limit or 10
    command = """
SELECT idUser AS id, username AS name, email FROM %s
WHERE name LIKE %s OR email LIKE %s
ORDER BY name
LIMIT %s
    """ % (self._table, self.PARAM_CHAR, self.PARAM_CHAR, limit)
    value = text.replace('%', '').strip()
    value = ''.join(value.split())
    value = '%' + value + '%'
    self.begin()
    data = self.execute_and_fetch(command, (value, value), as_dict=True)
    self.commit()
    return data

  def update_user_info(self, id, data):
    self.with_transaction(self.update_by_id, id, data)

  def delete_user(self, id):
    self.with_transaction(self.delete_by_id, id)






























