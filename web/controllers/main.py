#
import logging

from lib.database import UserStorage
from utils import BaseController



class Main(BaseController):

  TemplateName = 'index.html'

  def __init__(self, request):
    BaseController.__init__(self, request)
    self.user_db = UserStorage(connect=self.db)

  def index(self):
    data = dict()
    data['value'] = 'test_value'
    return self.Template.render(data)




