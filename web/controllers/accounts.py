#

import json
import uuid

from django.conf import settings
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm as _AuthForm
from django.contrib.auth.views import login as _login_view, logout as _logout
from django.utils.translation import  ugettext_lazy as _
from django.http import HttpResponseRedirect, HttpResponse

from lib.database import UserStorage
from lib.utils import format_form_errors
from lib.webauth import WebAuth

from utils import BaseController


class UserCreationForm(forms.Form):
  def __init__(self, *args, **kwargs):
    self._webauth = kwargs.pop('webauth', None)
    forms.Form.__init__(self, *args, **kwargs)

  error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
  }
  username = forms.RegexField(label=_("Username"),
        widget=forms.TextInput(attrs=dict({'class': '', 'placeholder': 'USERNAME'})),
        max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text=_("Required. 30 characters or fewer. Letters, digits and "
                      "@/./+/-/_ only."),
        error_messages={
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")}
  )
  password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput(attrs=dict({'class': '', 'placeholder': 'PASSWORD'})))
  password2 = forms.CharField(label=_("Conform password"),
        widget=forms.PasswordInput(attrs=dict({'class': '', 'placeholder': 'CONFIM PASSWORD'})),
        help_text=_("Enter the same password as above, for verification.")
  )
  email = forms.EmailField(label=_("E-mail"), max_length=250,
         widget=forms.TextInput(attrs=dict({'class': '', 'placeholder': 'EMAIL'}))
         )

  def clean_username(self):
    username = self.cleaned_data["username"]
    if not self._webauth.is_name_exists(username):
      return username
    raise forms.ValidationError(
      self.error_messages['duplicate_username'],
      code='duplicate_username',
    )

  def clean_password2(self):
    password1 = self.cleaned_data.get("password1")
    password2 = self.cleaned_data.get("password2")
    if password1 and password2 and password1 != password2:
      raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
      )
    return password2

  def save(self, commit=True):
    data = dict(
      username=self.cleaned_data['username'],
      password=self.cleaned_data['password1'],
      email=self.cleaned_data['email'],
      session_id=uuid.uuid4().hex
    )
    user = self._webauth.add_user(data, commit=True)
    return user

class AuthenticationForm(_AuthForm):
  username = forms.CharField(max_length=254, required=False)
  password = forms.CharField(
    max_length=254, required=False, label=_("Password"), widget=forms.PasswordInput
  )
  auth_provider = forms.CharField(max_length=32, required=False, widget=forms.HiddenInput())
  auth_id = forms.CharField(max_length=32, required=False, widget=forms.HiddenInput())
  access_token = forms.CharField(max_length=254, required=False, widget=forms.HiddenInput())

  error_messages = dict(
    _AuthForm.error_messages,
    invalid_user =  _("This user does not exist."),
  )

  def clean(self):
    username = self.cleaned_data.get('username')
    password = self.cleaned_data.get('password')
    auth_provider = self.cleaned_data.get('auth_provider')
    auth_id = self.cleaned_data.get('auth_id')
    access_token = self.cleaned_data.get('access_token')

    if auth_provider:
      self.user_cache = authenticate(
        auth_provider=auth_provider, auth_id=auth_id, access_token=access_token
      )
      if self.user_cache is None:
        raise forms.ValidationError(
          self.error_messages['invalid_user'],
          code='invalid_user'
        )
      elif not self.user_cache.is_active:
        raise forms.ValidationError(
          self.error_messages['inactive'],
          code='inactive',
        )
    else:
      if   not username:
	self._errors['username'] = self.error_class(['Field username required'])
      elif not password:
	self._errors['password'] = self.error_class(['Field password required'])
      else:
        super(AuthenticationForm, self).clean()
    return self.cleaned_data

class Accounts(BaseController):
  def __init__(self, request):
    BaseController.__init__(self, request)
    self.user = UserStorage(connect=self.db)

  def login(self):
#    self.log.error('login.method: %s' % self.request.method)
#    self.log.error('login: %s' % repr(self.request.POST))
    return _login_view(self.request,
      authentication_form=AuthenticationForm,
      template_name='accounts/login.html',
    )

  def is_valid_user(self):
    if self.request.method == 'POST':
      res = dict(error=False, message='')
      form = AuthenticationForm(self.request, data=self.request.POST)
      if not form.is_valid():
        res.update(error=True, message=format_form_errors(form))
    else:
      res = dict(error=True, message='must use POST method')
    res = json.dumps(res)
    return HttpResponse(res, content_type='application/json')

  def register(self):
    data = dict(error=None, message=None)
    response = None
    if self.request.method == 'POST':
      form = UserCreationForm(self.request.POST, webauth=WebAuth(self.user))
      data['form'] = form
      if form.is_valid():
        form.save()
        response = HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
    else:
      form = UserCreationForm()
      data['form'] = form
    if not response:
      response = self.Template.render(data, 'accounts/register.html')
    return response

  def logout(self):
    return _logout(self.request,
      next_page=settings.LOGOUT_REDIRECT_URL,
      template_name='accounts/logout.html'
    )