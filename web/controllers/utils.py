#
import logging
import django.db
from django.template import RequestContext, loader
from django.http import HttpResponse
from decorators import wrapped_view

from lib.utils import json_dumps


class Template(object):
  def __init__(self, master):
    self._master = master

  def render(self, data, template=None):
    template_name = template and template or self._master.TemplateName
    template = self._master.TemplateLoader.get_template(template_name)
    context = self._master.RequestContext(self._master.request, data)
    return template.render(context)

class BaseController(object):
  RequestContext = RequestContext
  TemplateLoader = loader
  TemplateName = None
  LoggerName = 'django.request'

  def __init__(self, request):
    self.request = request
    self.db = django.db.connections['main']
    self.log = logging.getLogger(self.LoggerName)
    self.Template = Template(self)

  def _login_required(self, __func, *args, **kwargs):
#    self.log.error('_login_required: %s' % repr(__func))
#    return __func(*args, **kwargs)
    return wrapped_view(self.request, __func, *args, **kwargs)

class JsonResponse(HttpResponse):
  def __init__(self, data, *args, **kwargs):
    kwargs.setdefault('content_type', 'application/json')
    data = json_dumps(data)
    HttpResponse.__init__(self, data, *args, **kwargs)

def to_utf8(text):
  if isinstance(text, unicode):
    text = text.encode('utf-8')
  return text

















