#
from utils import BaseController
from decorators import login_required


class Admin(BaseController):

  @login_required
  def admin_index(self):
    return self.Template.render({}, 'admin/index.html')