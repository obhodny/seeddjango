#
import inspect
from django.conf.urls import patterns, url, include
from django.http.response import HttpResponseBase
from django.http import HttpResponse
from django.shortcuts import redirect
#
from main import Main
from admin import Admin
from accounts import Accounts






from django.conf import settings
from django.conf.urls.static import static



CONTROLLERS = [ Accounts ]

def prepare_patterns(controllers):
  res = []
  for controller in controllers:
    controller_patterns = []
    for name in controller.__dict__:
      if not name.startswith('_'):
        method = getattr(controller, name)
        if inspect.ismethod(method):
          method_args = inspect.getargspec(method)
          url1 = r'^%s/%s' % (controller.__name__.lower(), name)
          len_args = len(method_args.args) - 1
          if len_args:
            url1 += (r'/([^\/]+)' * len_args)
          url11 = url1 + '$'
          url12 = url1 + '/$'
          controller_patterns.append([url11, Executer(controller, name)])
          controller_patterns.append([url12, Executer(controller, name)])
          if name == 'index':
            url1 = r'^%s/$' % controller.__name__.lower()
            controller_patterns.append([url1, Executer(controller, name)])
    res += create_patterns(controller_patterns)
  return res

def create_patterns(user_patterns):
  res = []
  for url1, executer in user_patterns:
    res.append(url(url1, executer))
  res = patterns('', *res)
  return res

class Executer(object):
  def __init__(self, controller, method):
    self.controller = controller
    self.method = method

  def __call__(self, request, *args, **kwargs):
    controller = self.controller(request)
    res = getattr(controller, self.method)(*args, **kwargs)
    if not isinstance(res, HttpResponseBase):
      res = HttpResponse(res)
    return res

urlpatterns = prepare_patterns(CONTROLLERS)
urlpatterns += patterns('',
  url('^$', Executer(Main, 'index')),
  url('^index$', Executer(Main, 'index')),
#
  url('^admin$',       Executer(Admin, 'admin_index')),
  url('^admin/$',       Executer(Admin, 'admin_index')),
  url('^admin/index$', Executer(Admin, 'admin_index'))
)# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
