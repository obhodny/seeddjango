#
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

#@register.filter
@register.simple_tag
@stringfilter
def if2(value, compare_value, res1, res2=None):
  if value == compare_value:
    res = res1
  else:
    res = res2
  return res
