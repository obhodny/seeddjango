"""
Django settings for epv project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

from lib import database_config as db_config

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'qj6lqe7vhuegy7z+wvj2!)8wmpj@#8p01y7+f5bcs!wtt1_9qm'

#SESSION_COOKIE_NAME = 'sessionid'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True
TEMPLATE_DIRS = [
  os.path.join(BASE_DIR, 'templates'),
#  os.path.join(BASE_DIR, 'video/templates')
]

#MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
#MEDIA_URL = '/media/'


ALLOWED_HOSTS = []

SESSION_COOKIE_NAME = 'test_sessionid'

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'seeddjango'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
#    'django.middleware.csrf.CsrfViewMiddleware',
    'lib.webmiddleware.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

AUTHENTICATION_BACKENDS = (
#  'django.contrib.auth.backends.ModelBackend',
  'lib.webauth.AuthBackend',
)


ROOT_URLCONF = 'seeddjango.urls'

WSGI_APPLICATION = 'seeddjango.wsgi.application'

#LOGIN_REDIRECT_URL = '/videos/profile'
LOGIN_REDIRECT_URL = '/index'
LOGOUT_REDIRECT_URL = '/index'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#    },
    'default': {
	'ENGINE': 'django.db.backends.mysql',
	'NAME': db_config.database_django,
	'USER': db_config.user,
	'PASSWORD': db_config.password,
	'HOST': db_config.host,
    },
    'main': {
	'ENGINE': 'django.db.backends.mysql',
	'NAME': db_config.database,
	'USER': db_config.user,
	'PASSWORD': db_config.password,
	'HOST': db_config.host,
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


CAPTCHA_IMAGE_SIZE = (158, 50)
CAPTCHA_FONT_SIZE = 40

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
#        'mail_admins': {
#            'level': 'ERROR',
#            'class': 'django.utils.log.AdminEmailHandler'
#        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/tmp/seeddjango/seeddjango.log',
        }
    },
    'loggers': {
        'django.request': {
#            'handlers': ['mail_admins', 'logfile'],
            'handlers': [ 'logfile' ],
            'level': 'INFO',
            'propagate': True,
        },
    }
}
