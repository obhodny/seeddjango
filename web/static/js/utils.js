
function call_server(url, data, error_text, is_get, send_files) {
    var request_type = is_get === true ? 'GET' : 'POST';
    data = data === undefined ? {} : data;
    if (error_text === undefined) {
	error_text = 'error execute ' + url;
    }
    var request = {
        url: url,
        type: request_type,
        data: data,
        dataType: 'json',
        async: false
    }
    if (request_type == 'POST' && send_files) {
	request.processData = false;
	request.contentType = false;
	request.cache = false;
    }
//    $.each(request, function(key, value){
//	console.log('key: '+key+', value: '+value);
//    });
    var res = $.ajax(request);
//      alert(res.responseText);
    var response = null;
    try {
        response = $.parseJSON(res.responseText);
        if (!response) { 
          response = {
            'error': false,
            'message': error_text + '\n\nClick OK to continue.\n\n'
          };
        }
    }
    catch (err) {
        console.log(res.responseText);
        response = {
          'error': true,
          'message': error_text + '\n\n' +
            'Error description: ' + err.message + '\n\n' +
            'Click OK to continue.\n\n'
        };
        response.message += '\n' + res.responseText;
    }
    return response;
}

function call_server_get(url, data, error_text) {
    return call_server(url, data, error_text, true);
}

function call_server_post(url, data, error_text, send_files) {
    return call_server(url, data, error_text, false, send_files);
}

function set_checkbox(checkbox, value) {
    if (value) {
	$(checkbox).attr('checked', 'checked');
    }
    else {
	$(checkbox).removeAttr('checked');
    }
}

function get_checkbox(checkbox) {
    var res = $(checkbox).is(':checked');
    res = res ? 1 : 0;
    return res;
}

function array_filter(data, func) {
  var res = [];
  for (var i in data) {
    var item = data[i];
    if (func(item)) { res.push(item); }
  }
  return res;
}

function array_index(data, func) {
  for(var i in data) {
    if (func(data[i])) { return i; }
  }
  return -1;
}

function array_find(data, func) {
  var index = array_index(data, func);
  if (index < 0) { return null; }
  else           { return data[index]; }
}

function array_map(data, func) {
  var res = [];
  for (i in data) { res.push(func(data[i], i)); }
  return res;
}

function cmp(a, b) {
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
}
